<?php
/**
 * @file
 * btsa_reader_contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function btsa_reader_contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reader_pts';
  $context->description = 'The PT\'s assigned to the current reader.';
  $context->tag = 'reader';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user' => 'user',
        'users*' => 'users*',
      ),
    ),
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'current',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-readers_assigned_pts2-block_1' => array(
          'module' => 'views',
          'delta' => 'readers_assigned_pts2-block_1',
          'region' => 'featured',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('The PT\'s assigned to the current reader.');
  t('reader');
  $export['reader_pts'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reader_user_profile';
  $context->description = 'Blocks and views that will be shown for readers';
  $context->tag = 'reader';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'current',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-pts_activity-block_2' => array(
          'module' => 'views',
          'delta' => 'pts_activity-block_2',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks and views that will be shown for readers');
  t('reader');
  $export['reader_user_profile'] = $context;

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_wide_reader_pts';
  $context->description = 'The PT\'s assigned to the current reader site wide context.';
  $context->tag = 'reader';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
    'user' => array(
      'values' => array(
        'portfolio reader' => 'portfolio reader',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-sp_assigned_pts-block_2' => array(
          'module' => 'views',
          'delta' => 'sp_assigned_pts-block_2',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('The PT\'s assigned to the current reader site wide context.');
  t('reader');
  $export['site_wide_reader_pts'] = $context;

  return $export;
}
