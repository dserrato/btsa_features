<?php
/**
 * @file
 * btsa_user_profile.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function btsa_user_profile_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'realname_pattern';
  $strongarm->value = '[user:profile-profile:field_first_name] [user:profile-profile:field_last_name]';
  $export['realname_pattern'] = $strongarm;

  return $export;
}
