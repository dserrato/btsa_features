<?php
/**
 * @file
 * btsa_user_profile.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function btsa_user_profile_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'user_profile_complete';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'User Profile Complete';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: User: Profile */
  $handler->display->display_options['relationships']['profile']['id'] = 'profile';
  $handler->display->display_options['relationships']['profile']['table'] = 'users';
  $handler->display->display_options['relationships']['profile']['field'] = 'profile';
  $handler->display->display_options['relationships']['profile']['bundle_types'] = array(
    'profile' => 'profile',
  );
  /* Relationship: Admin Info */
  $handler->display->display_options['relationships']['profile_1']['id'] = 'profile_1';
  $handler->display->display_options['relationships']['profile_1']['table'] = 'users';
  $handler->display->display_options['relationships']['profile_1']['field'] = 'profile';
  $handler->display->display_options['relationships']['profile_1']['ui_name'] = 'Admin Info';
  $handler->display->display_options['relationships']['profile_1']['required'] = TRUE;
  $handler->display->display_options['relationships']['profile_1']['bundle_types'] = array(
    'btsa_admin_info' => 'btsa_admin_info',
  );
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Field: Profile: Image */
  $handler->display->display_options['fields']['field_user_image']['id'] = 'field_user_image';
  $handler->display->display_options['fields']['field_user_image']['table'] = 'field_data_field_user_image';
  $handler->display->display_options['fields']['field_user_image']['field'] = 'field_user_image';
  $handler->display->display_options['fields']['field_user_image']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_user_image']['label'] = '';
  $handler->display->display_options['fields']['field_user_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_user_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_user_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: User: E-mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'users';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  $handler->display->display_options['fields']['mail']['label'] = '';
  $handler->display->display_options['fields']['mail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['mail']['link_to_user'] = '0';
  /* Field: Profile: SS */
  $handler->display->display_options['fields']['field_ss']['id'] = 'field_ss';
  $handler->display->display_options['fields']['field_ss']['table'] = 'field_data_field_ss';
  $handler->display->display_options['fields']['field_ss']['field'] = 'field_ss';
  $handler->display->display_options['fields']['field_ss']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_ss']['label'] = 'SS#';
  /* Field: Profile: DOB */
  $handler->display->display_options['fields']['field_dob']['id'] = 'field_dob';
  $handler->display->display_options['fields']['field_dob']['table'] = 'field_data_field_dob';
  $handler->display->display_options['fields']['field_dob']['field'] = 'field_dob';
  $handler->display->display_options['fields']['field_dob']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_dob']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Profile: Gender */
  $handler->display->display_options['fields']['field_gender']['id'] = 'field_gender';
  $handler->display->display_options['fields']['field_gender']['table'] = 'field_data_field_gender';
  $handler->display->display_options['fields']['field_gender']['field'] = 'field_gender';
  $handler->display->display_options['fields']['field_gender']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_gender']['type'] = 'text_plain';
  /* Field: Profile: Address */
  $handler->display->display_options['fields']['field_address']['id'] = 'field_address';
  $handler->display->display_options['fields']['field_address']['table'] = 'field_data_field_address';
  $handler->display->display_options['fields']['field_address']['field'] = 'field_address';
  $handler->display->display_options['fields']['field_address']['relationship'] = 'profile';
  /* Field: Profile: City */
  $handler->display->display_options['fields']['field_city']['id'] = 'field_city';
  $handler->display->display_options['fields']['field_city']['table'] = 'field_data_field_city';
  $handler->display->display_options['fields']['field_city']['field'] = 'field_city';
  $handler->display->display_options['fields']['field_city']['relationship'] = 'profile';
  /* Field: Profile: Phone */
  $handler->display->display_options['fields']['field_phone']['id'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['table'] = 'field_data_field_phone';
  $handler->display->display_options['fields']['field_phone']['field'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['relationship'] = 'profile';
  /* Field: Profile: ZIP */
  $handler->display->display_options['fields']['field_zip']['id'] = 'field_zip';
  $handler->display->display_options['fields']['field_zip']['table'] = 'field_data_field_zip';
  $handler->display->display_options['fields']['field_zip']['field'] = 'field_zip';
  $handler->display->display_options['fields']['field_zip']['relationship'] = 'profile';
  /* Field: Profile: District */
  $handler->display->display_options['fields']['field_district']['id'] = 'field_district';
  $handler->display->display_options['fields']['field_district']['table'] = 'field_data_field_district';
  $handler->display->display_options['fields']['field_district']['field'] = 'field_district';
  $handler->display->display_options['fields']['field_district']['relationship'] = 'profile';
  /* Field: Profile: School */
  $handler->display->display_options['fields']['field_school']['id'] = 'field_school';
  $handler->display->display_options['fields']['field_school']['table'] = 'field_data_field_school';
  $handler->display->display_options['fields']['field_school']['field'] = 'field_school';
  $handler->display->display_options['fields']['field_school']['relationship'] = 'profile';
  /* Field: Profile: School Address */
  $handler->display->display_options['fields']['field_school_address']['id'] = 'field_school_address';
  $handler->display->display_options['fields']['field_school_address']['table'] = 'field_data_field_school_address';
  $handler->display->display_options['fields']['field_school_address']['field'] = 'field_school_address';
  $handler->display->display_options['fields']['field_school_address']['relationship'] = 'profile';
  /* Field: Profile: School City */
  $handler->display->display_options['fields']['field_school_city']['id'] = 'field_school_city';
  $handler->display->display_options['fields']['field_school_city']['table'] = 'field_data_field_school_city';
  $handler->display->display_options['fields']['field_school_city']['field'] = 'field_school_city';
  $handler->display->display_options['fields']['field_school_city']['relationship'] = 'profile';
  /* Field: Profile: School ZIP */
  $handler->display->display_options['fields']['field_school_zip']['id'] = 'field_school_zip';
  $handler->display->display_options['fields']['field_school_zip']['table'] = 'field_data_field_school_zip';
  $handler->display->display_options['fields']['field_school_zip']['field'] = 'field_school_zip';
  $handler->display->display_options['fields']['field_school_zip']['relationship'] = 'profile';
  /* Field: Profile: School Phone */
  $handler->display->display_options['fields']['field_school_phone']['id'] = 'field_school_phone';
  $handler->display->display_options['fields']['field_school_phone']['table'] = 'field_data_field_school_phone';
  $handler->display->display_options['fields']['field_school_phone']['field'] = 'field_school_phone';
  $handler->display->display_options['fields']['field_school_phone']['relationship'] = 'profile';
  /* Field: Profile: Site Administrator */
  $handler->display->display_options['fields']['field_site_administrator']['id'] = 'field_site_administrator';
  $handler->display->display_options['fields']['field_site_administrator']['table'] = 'field_data_field_site_administrator';
  $handler->display->display_options['fields']['field_site_administrator']['field'] = 'field_site_administrator';
  $handler->display->display_options['fields']['field_site_administrator']['relationship'] = 'profile';
  /* Field: Profile: Subject */
  $handler->display->display_options['fields']['field_subject']['id'] = 'field_subject';
  $handler->display->display_options['fields']['field_subject']['table'] = 'field_data_field_subject';
  $handler->display->display_options['fields']['field_subject']['field'] = 'field_subject';
  $handler->display->display_options['fields']['field_subject']['relationship'] = 'profile';
  /* Field: Profile: Grade */
  $handler->display->display_options['fields']['field_grade']['id'] = 'field_grade';
  $handler->display->display_options['fields']['field_grade']['table'] = 'field_data_field_grade';
  $handler->display->display_options['fields']['field_grade']['field'] = 'field_grade';
  $handler->display->display_options['fields']['field_grade']['relationship'] = 'profile';
  /* Field: Profile: Hire Date */
  $handler->display->display_options['fields']['field_hire_date']['id'] = 'field_hire_date';
  $handler->display->display_options['fields']['field_hire_date']['table'] = 'field_data_field_hire_date';
  $handler->display->display_options['fields']['field_hire_date']['field'] = 'field_hire_date';
  $handler->display->display_options['fields']['field_hire_date']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_hire_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Profile: Induction Program */
  $handler->display->display_options['fields']['field_induction_program']['id'] = 'field_induction_program';
  $handler->display->display_options['fields']['field_induction_program']['table'] = 'field_data_field_induction_program';
  $handler->display->display_options['fields']['field_induction_program']['field'] = 'field_induction_program';
  $handler->display->display_options['fields']['field_induction_program']['relationship'] = 'profile';
  /* Field: Profile: Date NOE Received */
  $handler->display->display_options['fields']['field_date_noe_received']['id'] = 'field_date_noe_received';
  $handler->display->display_options['fields']['field_date_noe_received']['table'] = 'field_data_field_date_noe_received';
  $handler->display->display_options['fields']['field_date_noe_received']['field'] = 'field_date_noe_received';
  $handler->display->display_options['fields']['field_date_noe_received']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_date_noe_received']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Profile: Program Status */
  $handler->display->display_options['fields']['field_program_status']['id'] = 'field_program_status';
  $handler->display->display_options['fields']['field_program_status']['table'] = 'field_data_field_program_status';
  $handler->display->display_options['fields']['field_program_status']['field'] = 'field_program_status';
  $handler->display->display_options['fields']['field_program_status']['relationship'] = 'profile';
  /* Field: Profile: Credential */
  $handler->display->display_options['fields']['field_credential']['id'] = 'field_credential';
  $handler->display->display_options['fields']['field_credential']['table'] = 'field_data_field_credential';
  $handler->display->display_options['fields']['field_credential']['field'] = 'field_credential';
  $handler->display->display_options['fields']['field_credential']['relationship'] = 'profile';
  /* Field: Profile: Credential Term */
  $handler->display->display_options['fields']['field_credential_term']['id'] = 'field_credential_term';
  $handler->display->display_options['fields']['field_credential_term']['table'] = 'field_data_field_credential_term';
  $handler->display->display_options['fields']['field_credential_term']['field'] = 'field_credential_term';
  $handler->display->display_options['fields']['field_credential_term']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_credential_term']['delta_offset'] = '0';
  /* Field: Profile: Credential Type */
  $handler->display->display_options['fields']['field_credential_type']['id'] = 'field_credential_type';
  $handler->display->display_options['fields']['field_credential_type']['table'] = 'field_data_field_credential_type';
  $handler->display->display_options['fields']['field_credential_type']['field'] = 'field_credential_type';
  $handler->display->display_options['fields']['field_credential_type']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_credential_type']['delta_offset'] = '0';
  /* Field: Profile: Credential Expiration */
  $handler->display->display_options['fields']['field_credential_expiration']['id'] = 'field_credential_expiration';
  $handler->display->display_options['fields']['field_credential_expiration']['table'] = 'field_data_field_credential_expiration';
  $handler->display->display_options['fields']['field_credential_expiration']['field'] = 'field_credential_expiration';
  $handler->display->display_options['fields']['field_credential_expiration']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_credential_expiration']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Profile: Leave Status */
  $handler->display->display_options['fields']['field_on_leave']['id'] = 'field_on_leave';
  $handler->display->display_options['fields']['field_on_leave']['table'] = 'field_data_field_on_leave';
  $handler->display->display_options['fields']['field_on_leave']['field'] = 'field_on_leave';
  $handler->display->display_options['fields']['field_on_leave']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_on_leave']['hide_empty'] = TRUE;
  /* Field: Profile: Induction Coach */
  $handler->display->display_options['fields']['field_pt_support_provider']['id'] = 'field_pt_support_provider';
  $handler->display->display_options['fields']['field_pt_support_provider']['table'] = 'field_data_field_pt_support_provider';
  $handler->display->display_options['fields']['field_pt_support_provider']['field'] = 'field_pt_support_provider';
  $handler->display->display_options['fields']['field_pt_support_provider']['relationship'] = 'profile';
  $handler->display->display_options['fields']['field_pt_support_provider']['settings'] = array(
    'link' => 0,
  );
  /* Field: Profile: Assigned Reader */
  $handler->display->display_options['fields']['field_pt_reader']['id'] = 'field_pt_reader';
  $handler->display->display_options['fields']['field_pt_reader']['table'] = 'field_data_field_pt_reader';
  $handler->display->display_options['fields']['field_pt_reader']['field'] = 'field_pt_reader';
  $handler->display->display_options['fields']['field_pt_reader']['relationship'] = 'profile_1';
  $handler->display->display_options['fields']['field_pt_reader']['settings'] = array(
    'link' => 0,
  );
  /* Field: Profile: Administration Notes */
  $handler->display->display_options['fields']['field_administration_notes']['id'] = 'field_administration_notes';
  $handler->display->display_options['fields']['field_administration_notes']['table'] = 'field_data_field_administration_notes';
  $handler->display->display_options['fields']['field_administration_notes']['field'] = 'field_administration_notes';
  $handler->display->display_options['fields']['field_administration_notes']['relationship'] = 'profile_1';
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['uid']['validate']['type'] = 'user';
  $handler->display->display_options['arguments']['uid']['validate_options']['type'] = 'either';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['user_profile_complete'] = $view;

  return $export;
}
