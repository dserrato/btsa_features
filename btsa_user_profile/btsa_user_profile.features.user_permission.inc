<?php
/**
 * @file
 * btsa_user_profile.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function btsa_user_profile_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_pt_support_provider'.
  $permissions['create field_pt_support_provider'] = array(
    'name' => 'create field_pt_support_provider',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit any profile profile'.
  $permissions['edit any profile profile'] = array(
    'name' => 'edit any profile profile',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'edit field_pt_support_provider'.
  $permissions['edit field_pt_support_provider'] = array(
    'name' => 'edit field_pt_support_provider',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_pt_support_provider'.
  $permissions['edit own field_pt_support_provider'] = array(
    'name' => 'edit own field_pt_support_provider',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own profile profile'.
  $permissions['edit own profile profile'] = array(
    'name' => 'edit own profile profile',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view any profile profile'.
  $permissions['view any profile profile'] = array(
    'name' => 'view any profile profile',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
      'induction coach' => 'induction coach',
      'portfolio reader' => 'portfolio reader',
    ),
    'module' => 'profile2',
  );

  // Exported permission: 'view field_pt_support_provider'.
  $permissions['view field_pt_support_provider'] = array(
    'name' => 'view field_pt_support_provider',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_pt_support_provider'.
  $permissions['view own field_pt_support_provider'] = array(
    'name' => 'view own field_pt_support_provider',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own profile profile'.
  $permissions['view own profile profile'] = array(
    'name' => 'view own profile profile',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
      'induction coach' => 'induction coach',
      'portfolio reader' => 'portfolio reader',
    ),
    'module' => 'profile2',
  );

  return $permissions;
}
