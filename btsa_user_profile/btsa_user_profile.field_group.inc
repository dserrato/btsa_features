<?php
/**
 * @file
 * btsa_user_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function btsa_user_profile_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_credential_information|profile2|profile|form';
  $field_group->group_name = 'group_credential_information';
  $field_group->entity_type = 'profile2';
  $field_group->bundle = 'profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Credential Information',
    'weight' => '28',
    'children' => array(
      0 => 'field_credential',
      1 => 'field_credential_expiration',
      2 => 'field_credential_term',
      3 => 'field_credential_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-credential-information field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_credential_information|profile2|profile|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Credential Information');

  return $field_groups;
}
