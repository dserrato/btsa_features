<?php
/**
 * @file
 * btsa_support_provider_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function btsa_support_provider_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'sp_assigned_pts';
  $view->description = '';
  $view->tag = 'sp';
  $view->base_table = 'profile';
  $view->human_name = 'SP assigned PTs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'My Participating Teachers';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
    7 => '7',
    6 => '6',
    4 => '4',
    5 => '5',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Profile: User uid */
  $handler->display->display_options['relationships']['user']['id'] = 'user';
  $handler->display->display_options['relationships']['user']['table'] = 'profile';
  $handler->display->display_options['relationships']['user']['field'] = 'user';
  /* Relationship: User: Profile */
  $handler->display->display_options['relationships']['profile']['id'] = 'profile';
  $handler->display->display_options['relationships']['profile']['table'] = 'users';
  $handler->display->display_options['relationships']['profile']['field'] = 'profile';
  $handler->display->display_options['relationships']['profile']['relationship'] = 'user';
  $handler->display->display_options['relationships']['profile']['bundle_types'] = array(
    'btsa_admin_info' => 'btsa_admin_info',
  );
  /* Field: User: Name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'users';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['relationship'] = 'user';
  /* Field: User: E-mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'users';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  $handler->display->display_options['fields']['mail']['relationship'] = 'user';
  /* Field: User: Link */
  $handler->display->display_options['fields']['view_user']['id'] = 'view_user';
  $handler->display->display_options['fields']['view_user']['table'] = 'users';
  $handler->display->display_options['fields']['view_user']['field'] = 'view_user';
  $handler->display->display_options['fields']['view_user']['relationship'] = 'user';
  $handler->display->display_options['fields']['view_user']['text'] = 'Teacher\'s Profile/FactDocs';
  /* Contextual filter: Profile: Induction Coach (field_pt_support_provider) */
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['id'] = 'field_pt_support_provider_target_id';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['table'] = 'field_data_field_pt_support_provider';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['field'] = 'field_pt_support_provider_target_id';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: User: Roles */
  $handler->display->display_options['filters']['rid']['id'] = 'rid';
  $handler->display->display_options['filters']['rid']['table'] = 'users_roles';
  $handler->display->display_options['filters']['rid']['field'] = 'rid';
  $handler->display->display_options['filters']['rid']['relationship'] = 'user';
  $handler->display->display_options['filters']['rid']['value'] = array(
    4 => '4',
  );
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['relationship'] = 'user';
  $handler->display->display_options['filters']['status']['value'] = '1';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');

  /* Display: Simple List of Support Provider's PT's */
  $handler = $view->new_display('block', 'Simple List of Support Provider\'s PT\'s', 'block_2');
  $handler->display->display_options['display_description'] = 'Simple List of Support Provider\'s PT\'s';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'users';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['relationship'] = 'user';
  $handler->display->display_options['fields']['name_1']['label'] = '';
  $handler->display->display_options['fields']['name_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Profile: Induction Coach (field_pt_support_provider) */
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['id'] = 'field_pt_support_provider_target_id';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['table'] = 'field_data_field_pt_support_provider';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['field'] = 'field_pt_support_provider_target_id';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary_options']['items_per_page'] = '25';

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'name_1' => 'name_1',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'users';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  $handler->display->display_options['fields']['name_1']['relationship'] = 'user';
  $handler->display->display_options['fields']['name_1']['label'] = '';
  $handler->display->display_options['fields']['name_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name_1']['link_to_user'] = FALSE;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Profile: Induction Coach (field_pt_support_provider) */
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['id'] = 'field_pt_support_provider_target_id';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['table'] = 'field_data_field_pt_support_provider';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['field'] = 'field_pt_support_provider_target_id';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_pt_support_provider_target_id']['summary_options']['items_per_page'] = '25';
  $export['sp_assigned_pts'] = $view;

  return $export;
}
