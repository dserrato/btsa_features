<?php
/**
 * @file
 * btsa_support_provider_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function btsa_support_provider_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
