<?php
/**
 * @file
 * btsa_admin_info.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function btsa_admin_info_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_administration_notes'.
  $permissions['create field_administration_notes'] = array(
    'name' => 'create field_administration_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_pt_reader'.
  $permissions['create field_pt_reader'] = array(
    'name' => 'create field_pt_reader',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_administration_notes'.
  $permissions['edit field_administration_notes'] = array(
    'name' => 'edit field_administration_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_pt_reader'.
  $permissions['edit field_pt_reader'] = array(
    'name' => 'edit field_pt_reader',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_administration_notes'.
  $permissions['edit own field_administration_notes'] = array(
    'name' => 'edit own field_administration_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_pt_reader'.
  $permissions['edit own field_pt_reader'] = array(
    'name' => 'edit own field_pt_reader',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_administration_notes'.
  $permissions['view field_administration_notes'] = array(
    'name' => 'view field_administration_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_pt_reader'.
  $permissions['view field_pt_reader'] = array(
    'name' => 'view field_pt_reader',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_administration_notes'.
  $permissions['view own field_administration_notes'] = array(
    'name' => 'view own field_administration_notes',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_pt_reader'.
  $permissions['view own field_pt_reader'] = array(
    'name' => 'view own field_pt_reader',
    'roles' => array(
      'administrator' => 'administrator',
      'induction administrator' => 'induction administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
