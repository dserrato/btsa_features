<?php
/**
 * @file
 * btsa_admin_info.features.inc
 */

/**
 * Implements hook_default_profile2_type().
 */
function btsa_admin_info_default_profile2_type() {
  $items = array();
  $items['btsa_admin_info'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "btsa_admin_info",
    "label" : "Administration Information",
    "weight" : "0",
    "data" : { "registration" : 0 }
  }');
  return $items;
}
