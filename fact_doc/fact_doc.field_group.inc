<?php
/**
 * @file
 * fact_doc.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fact_doc_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_supporting_documents|node|fact_doc|form';
  $field_group->group_name = 'group_supporting_documents';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'fact_doc';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Supporting Documents',
    'weight' => '9',
    'children' => array(
      0 => 'field_fact_doc_supporting_docs',
      1 => 'field_fact_doc_supporting_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-supporting-documents field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_supporting_documents|node|fact_doc|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Supporting Documents');

  return $field_groups;
}
