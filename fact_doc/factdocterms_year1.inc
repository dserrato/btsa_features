<?php


$program_status = $profile['profile']->field_induction_program['und'][0]['value'];
      
  if($program_status == education_specialist || general_education) {
  
	  $factdocterms = array(
	    // Individualized Learning Plan (ILP)
	    'Initial ILP' => array(
	      'year' => 1,
	      'module' => 216,
	      'name' => 188,
	      ),
	    'Initial Administrator Collaborative' => array(
	      'year' => 1,
	      'module' => 216,
	      'name' => 189,
	      ),
	    'Final ILP' => array(
	      'year' => 1,
	      'module' => 216,
	      'name' => 208,
	      ),


	    // Action Research
	    'Initial Action Research' => array(
	      'year' => 1,
	      'module' => 177,
	      'name' => 209,
	    ),
	    'Final Action Research' => array(
	    	'year' => 1,
	    	'module' => 177,
	    	'name' => 210,
	    ),
	    'Final Administrator Collaborative' => array(
	      'year' => 1,
	      'module' => 177,
	      'name' => 192,
	    ),
	    'Presenting Your Research' => array(
	      'year' => 1,
	      'module' => 177,
	      'name' => 211,
	    ),

	    // Developing as a Professional Educator

	    'Peer Observation' => array(
	        'year' => 1,
	        'module' => 207,
	        'name' => 212,
	    ),
	    'Observation by Coach' => array(
	        'year' => 1,
	        'module' => 207,
	        'name' => 213,
	    ),
	    'Observation of Choice' => array(
	        'year' => 1,
	        'module' => 207,
	        'name' => 214,
	    ),
	    'Observation of Choice 2' => array(
	        'year' => 1,
	        'module' => 207,
	        'name' => 215,
	    ),
	  );
  }