<?php
/**
 * @file
 * fact_doc.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function fact_doc_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'fact_doc_log_messages';
  $context->description = 'Context for Fact Doc nodes to show the log of comments';
  $context->tag = 'facdoc';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'fact_doc' => 'fact_doc',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-fact_doc_log_messages-block' => array(
          'module' => 'views',
          'delta' => 'fact_doc_log_messages-block',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Context for Fact Doc nodes to show the log of comments');
  t('facdoc');
  $export['fact_doc_log_messages'] = $context;

  return $export;
}
