<?php

/**
 * @file
 * provides the dashboard for participating teachers
 */

function  fact_doc_factdocterms($profile) {
  $btsa_status = $profile['profile']->field_program_status['und'][0]['value'];

    switch ($btsa_status) {
      case 'year1':
        include_once('factdocterms_year1.inc');    
      break;
      case 'year2':
        include_once('factdocterms_year2.inc');
      break;
      case 'eco':
        include_once('factdocterms_eco.inc');
    break;
    }
  return $factdocterms;
}

/**
 * Implements fact_doc_dashboard_content()
 * Expects $user object and the user $role
 * Gets factdoc nodes for the current user
 * if the user is a PT or the user from the url
 */
function fact_doc_dashboard_content($user, $profile, $role) {

  $factdocterms = fact_doc_factdocterms($profile);

  $result = db_query("SELECT node.title AS node_title, node.nid AS nid, node_revision_states.state AS node_revision_states_state, node.created AS node_created
  FROM
  {node} node
  LEFT JOIN {node_revision_states} node_revision_states ON node.vid = node_revision_states.vid
  WHERE (( (node.uid = '$user->uid') )AND( (node.type = 'fact_doc') )AND(( (node.status = '1') )))
  ORDER BY node_title ASC");

  $records = array();
  foreach ($result as $record) {

    $node = node_load($record->nid);
    $year = field_get_items('node', $node, 'field_fact_doc_year');
    $module = field_get_items('node', $node, 'field_fact_doc_mod');
    $name = field_get_items('node', $node, 'field_fact_doc_name');
    $record->year_value = field_view_value('node', $node, 'field_fact_doc_year', $year[0]);
    $record->module_value = field_view_value('node', $node, 'field_fact_doc_mod', $module[0]);
    $record->name_value = field_view_value('node', $node, 'field_fact_doc_name', $name[0]);
    $record->year = $year[0]['tid'];
    $record->module = $module[0]['tid'];
    $record->name = $name[0]['tid'];

    foreach ($factdocterms as $key => $value) {

      if ($value['year'] == $record->year && $value['module'] == $record->module && $value['name'] == $record->name) {

        $value['text'] = $record->node_title;
        $value['state'] = $record->node_revision_states_state;
        $value['url'] = '/node/' . $record->nid;
        $value['year'] = $record->year;
        $value['module'] = $record->module;
        $value['module_value'] = $record->module_value;
        $value['name'] = $record->name;
        $value['submited'] = 1;
        $value['nid'] = $record->nid;
        $factdocterms[$key] = $value;
      }
      $name = taxonomy_term_load($value['name']);
      $name = $name->name;

      if (!isset($factdocterms[$key]['text']) && empty($factdocterms[$key]['text'])) {
        $factdocterms[$key]['text'] = $name;
      }

      if (!isset($factdocterms[$key]['state']) && empty($factdocterms[$key]['state'])) {
        $factdocterms[$key]['state'] = 'not submitted';
      }

      if (!isset($factdocterms[$key]['url']) && empty($factdocterms[$key]['url'])) {
        $factdocterms[$key]['url'] = '/node/add/fact-doc?edit[field_fact_doc_year][und]=' . $value['year'] .
        '&edit[field_fact_doc_mod][und]=' . $value['module'] . '&edit[field_fact_doc_name][und]=' . $value['name'];
      }

    }
  }

  // check for no values
  //  name, state, url, text
  foreach ($factdocterms as $key => $value) {

    if (!isset($factdocterms[$key]['text']) && empty($factdocterms[$key]['text'])) {
      $name = taxonomy_term_load($value['name']);
      $name = $name->name;
      $factdocterms[$key]['text'] =  $name;
    }

    if (!isset($factdocterms[$key]['state']) && empty($factdocterms[$key]['state'])) {
      $factdocterms[$key]['state'] = 'not submitted';
    }

    if (!isset($factdocterms[$key]['url']) && empty($factdocterms[$key]['url'])) {
      $factdocterms[$key]['url'] = '/node/add/fact-doc?edit[field_fact_doc_year][und]=' . $value['year'] .
      '&edit[field_fact_doc_mod][und]=' . $value['module'] . '&edit[field_fact_doc_name][und]=' . $value['name'];
    }
  }

  if ($role == 'teacher') {
    $output = factdoc_teacher_dashboard($factdocterms);
  }

  if ($role == 'reader') {
    $output = factdoc_reader_dashboard($factdocterms, $role);
  }

  if ($role == 'induction coach') {
    $output = factdoc_reader_dashboard($factdocterms, $role);
  }

  if ($role == 'btsa administrator') {
    $output = factdoc_reader_dashboard($factdocterms, $role);
  }

  return $output;
}

function factdoc_teacher_dashboard($factdocterms) {

  $output = '<table class="teacher">';

  $moduleheader = NULL;
  foreach ($factdocterms AS $factdocterm => $value) {

    $term = taxonomy_term_load($value['module']);

    // Group by module
    if ($moduleheader != $value['module']) {

      $output .= '<tr>
                    <th colspan="3">' . $term->name . '</th>
                  </tr>
                  <tr>';
    }
    // add state class to rows
    $stateclass = $value['state'];
    $stateclass = strtolower($stateclass);
    $stateclass = str_replace(' ', '-', $stateclass);
    $output .= '<tr class="' . $stateclass . '">';
    $moduleheader = $value['module'];

    if ($value['state'] == 'not submitted') {
      $output .=  '<td><a href="' . $value['url'] . '">Create ' . $value['text'] . '</a></td>
                     <td>' . drupal_ucfirst($value['state']) . '</td>
                   </tr>';
    }

    else {
      $output .=  '<td><a href="' . $value['url'] . '">' . $value['text'] . '</a></td>
                     <td>' . drupal_ucfirst($value['state']) . '</td>
                   </tr>';
    }
  }

  $output .= '</table>';

  return  $output;

}

function factdoc_reader_dashboard($factdocterms, $role) {
  $output = '<table class="reader">';

  $moduleheader = NULL;
  foreach ($factdocterms AS $factdocterm => $value) {

    $term = taxonomy_term_load($value['module']);

    // Group by module
    if ($moduleheader != $value['module']) {
      if ($role == 'reader') {
        $output .= '<tr>
                      <th colspan="3">' . $term->name . '</th>
                    </tr>';
      }

      else {
        $output .= '<tr>
                      <th colspan="2">' . $term->name . '</th>
                    </tr>';
      }
    }
    // add state class to rows
    $stateclass = $value['state'];
    $stateclass = strtolower($stateclass);
    $stateclass = str_replace(' ', '-', $stateclass);
    $output .= '<tr class="' . $stateclass . '">';
    $moduleheader = $value['module'];
    if ($value['state'] == 'not submitted') {
      if ($role == 'reader') {
        $output .=    '<td>' . $value['text'] . '</td>
                       <td>' . drupal_ucfirst($value['state']) . '</td>
                       <td>Not submitted</td>
                     </tr>';
      }

      else {
        $output .=    '<td>' . $value['text'] . '</td>
                       <td>' . drupal_ucfirst($value['state']) . '</td>
                     </tr>';
      }
    }
    elseif ($value['state'] == 'complete' || $value['state'] == 'does not meet standards') {
      if ($role == 'reader') {
        $output .=    '<td><a href="' . $value['url'] . '">' . $value['text'] . '</a></td>
                       <td>' . drupal_ucfirst($value['state']) . '</td>
                       <td><a href="' . $value['url'] . '/workflow">Workflow history</a></td>
                     </tr>';
      }

      else {
        $output .=    '<td><a href="' . $value['url'] . '">' . $value['text'] . '</a></td>
                       <td>' . drupal_ucfirst($value['state']) . '</td>
                     </tr>';
      }
    }
    else {

      $states = state_flow_get_revisions($value['nid']);
      $workflowlink = '/node/' . $states[0]->nid . '/revisions/' . $states[0]->vid  . '/workflow/';
      if ($role == 'reader') {
        $output .=  '<td><a href="' . $value['url'] . '">' . $value['text'] . '</a></td>
                       <td>' . drupal_ucfirst($value['state']) . '</td>
                       <td>' . '<a href="' . $workflowlink . 'approve">Approve</a> | <a href="' . $workflowlink
                             . 'needs%20more%20work">Needs more work</a></td>
                     </tr>';
      }

      else {
        $output .=  '<td><a href="' . $value['url'] . '">' . $value['text'] . '</a></td>
                       <td>' . drupal_ucfirst($value['state']) . '</td>
                     </tr>';
      }
    }
  }

  $output .= '</table>';

  return  $output;

}
