<?php
/**
 * @file
 * fact_doc.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function fact_doc_taxonomy_default_vocabularies() {
  return array(
    'factdoc_module' => array(
      'name' => 'FactDoc Module',
      'machine_name' => 'factdoc_module',
      'description' => 'BTSA Modules',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'factdoc_name' => array(
      'name' => 'FactDoc Name',
      'machine_name' => 'factdoc_name',
      'description' => 'FactDoc Names',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'factdoc_year' => array(
      'name' => 'FactDoc Year',
      'machine_name' => 'factdoc_year',
      'description' => 'The BTSA year of participating teacher.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
