<?php
/**
 * @file
 * user_registration.features.inc
 */

/**
 * Implements hook_views_api().
 */
function user_registration_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
