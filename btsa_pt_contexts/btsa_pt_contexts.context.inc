<?php
/**
 * @file
 * btsa_pt_contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function btsa_pt_contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'pt_dashboard';
  $context->description = 'Participating Teacher Dashboard';
  $context->tag = 'pt';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user' => 'user',
        'users/*' => 'users/*',
      ),
    ),
    'user' => array(
      'values' => array(
        'participating teacher' => 'participating teacher',
      ),
    ),
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'current',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'fact_doc-dashboard' => array(
          'module' => 'fact_doc',
          'delta' => 'dashboard',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Participating Teacher Dashboard');
  t('pt');
  $export['pt_dashboard'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'pt_documents';
  $context->description = 'Display Documents block on PT dashboard';
  $context->tag = 'pt';
  $context->conditions = array(
    'profile_role' => array(
      'values' => array(
        'participating teacher' => 'participating teacher',
      ),
    ),
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-user_documents-block' => array(
          'module' => 'views',
          'delta' => 'user_documents-block',
          'region' => 'dash_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Display Documents block on PT dashboard');
  t('pt');
  $export['pt_documents'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'pt_profile';
  $context->description = 'Participating Teacher Profile';
  $context->tag = 'pt';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user/~/~' => 'user/~/~',
        'user/*' => 'user/*',
      ),
    ),
    'user' => array(
      'values' => array(
        'administrator' => 'administrator',
        'btsa administrator' => 'btsa administrator',
        'participating teacher' => 'participating teacher',
        'portfolio reader' => 'portfolio reader',
        'induction coach' => 'induction coach',
      ),
    ),
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'other',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'fact_doc-reader_dashboard' => array(
          'module' => 'fact_doc',
          'delta' => 'reader_dashboard',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Participating Teacher Profile');
  t('pt');
  $export['pt_profile'] = $context;

  return $export;
}
