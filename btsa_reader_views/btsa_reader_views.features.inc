<?php
/**
 * @file
 * btsa_reader_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function btsa_reader_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
