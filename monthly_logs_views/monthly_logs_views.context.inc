<?php
/**
 * @file
 * monthly_logs_views.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function monthly_logs_views_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user-defaults';
  $context->description = 'Blocks and views that will be shown for users';
  $context->tag = 'General';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-user_profile_complete-block' => array(
          'module' => 'views',
          'delta' => 'user_profile_complete-block',
          'region' => 'content',
          'weight' => '-21',
        ),
        'views-user_registrations-block' => array(
          'module' => 'views',
          'delta' => 'user_registrations-block',
          'region' => 'dash_first',
          'weight' => '-10',
        ),
        'views-monthly_logs-block' => array(
          'module' => 'views',
          'delta' => 'monthly_logs-block',
          'region' => 'dash_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks and views that will be shown for users');
  t('General');
  $export['user-defaults'] = $context;

  return $export;
}
