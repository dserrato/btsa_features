<?php
/**
 * @file
 * monthly_logs_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function monthly_logs_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'monthly_log_link';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'Monthly Log Link';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Monthly Log Link';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    6 => '6',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: User: Profile */
  $handler->display->display_options['relationships']['profile']['id'] = 'profile';
  $handler->display->display_options['relationships']['profile']['table'] = 'users';
  $handler->display->display_options['relationships']['profile']['field'] = 'profile';
  $handler->display->display_options['relationships']['profile']['bundle_types'] = array(
    'profile' => 'profile',
  );
  /* Field: User: Uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'users';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = '';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uid']['link_to_user'] = FALSE;
  /* Field: Profile: Profile ID */
  $handler->display->display_options['fields']['pid']['id'] = 'pid';
  $handler->display->display_options['fields']['pid']['table'] = 'profile';
  $handler->display->display_options['fields']['pid']['field'] = 'pid';
  $handler->display->display_options['fields']['pid']['relationship'] = 'profile';
  $handler->display->display_options['fields']['pid']['label'] = '';
  $handler->display->display_options['fields']['pid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['pid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['pid']['separator'] = '';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="/node/add/monthly-log?edit[field_pt_auth_monthly_log][und]=[pid]" class="button tiny">Add Monthly Log</a>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['uid']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['uid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['uid']['validate']['type'] = 'user';
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['monthly_log_link'] = $view;

  $view = new view();
  $view->name = 'monthly_logs';
  $view->description = 'List of Monthly Logs';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Monthly Logs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Monthly Logs';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'edit_node' => 'edit_node',
    'field_monthly_log_file' => 'field_monthly_log_file',
    'field_mlog_participation_year' => 'field_mlog_participation_year',
    'field_month' => 'field_month',
    'uid' => 'uid',
    'field_monthly_log_status' => 'field_monthly_log_status',
    'realname' => 'realname',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_monthly_log_file' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_mlog_participation_year' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_month' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_monthly_log_status' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'realname' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = TRUE;
  $handler->display->display_options['header']['area']['content'] = '<a href="/node/add/monthly-log" class="button tiny">Add Monthly Log</a>';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  $handler->display->display_options['header']['area']['tokenize'] = TRUE;
  $handler->display->display_options['header']['area']['roles_fieldset'] = array(
    'roles' => array(
      'induction coach' => 'induction coach',
      'anonymous user' => 0,
      'authenticated user' => 0,
      'complete' => 0,
      'participating teacher' => 0,
      'portfolio reader' => 0,
      'induction administrator' => 0,
      'superuser' => 0,
      'administrator' => 0,
    ),
  );
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no monthly logs submitted for this user. ';
  $handler->display->display_options['empty']['area']['format'] = 'wysiwyg';
  $handler->display->display_options['empty']['area']['roles_fieldset'] = array(
    'roles' => array(
      'anonymous user' => 0,
      'authenticated user' => 0,
      'complete' => 0,
      'participating teacher' => 0,
      'induction coach' => 0,
      'portfolio reader' => 0,
      'induction administrator' => 0,
      'superuser' => 0,
      'administrator' => 0,
    ),
  );
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_pt_auth_monthly_log_target_id']['id'] = 'field_pt_auth_monthly_log_target_id';
  $handler->display->display_options['relationships']['field_pt_auth_monthly_log_target_id']['table'] = 'field_data_field_pt_auth_monthly_log';
  $handler->display->display_options['relationships']['field_pt_auth_monthly_log_target_id']['field'] = 'field_pt_auth_monthly_log_target_id';
  /* Relationship: Profile: User uid */
  $handler->display->display_options['relationships']['user']['id'] = 'user';
  $handler->display->display_options['relationships']['user']['table'] = 'profile';
  $handler->display->display_options['relationships']['user']['field'] = 'user';
  $handler->display->display_options['relationships']['user']['relationship'] = 'field_pt_auth_monthly_log_target_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = '';
  $handler->display->display_options['fields']['edit_node']['exclude'] = TRUE;
  $handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_node']['text'] = '(EDIT)';
  /* Field: Content: Monthly Log File */
  $handler->display->display_options['fields']['field_monthly_log_file']['id'] = 'field_monthly_log_file';
  $handler->display->display_options['fields']['field_monthly_log_file']['table'] = 'field_data_field_monthly_log_file';
  $handler->display->display_options['fields']['field_monthly_log_file']['field'] = 'field_monthly_log_file';
  $handler->display->display_options['fields']['field_monthly_log_file']['label'] = '';
  $handler->display->display_options['fields']['field_monthly_log_file']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_monthly_log_file']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_monthly_log_file']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_monthly_log_file']['type'] = 'file_url_plain';
  /* Field: Content: Participating School Year */
  $handler->display->display_options['fields']['field_mlog_participation_year']['id'] = 'field_mlog_participation_year';
  $handler->display->display_options['fields']['field_mlog_participation_year']['table'] = 'field_data_field_mlog_participation_year';
  $handler->display->display_options['fields']['field_mlog_participation_year']['field'] = 'field_mlog_participation_year';
  $handler->display->display_options['fields']['field_mlog_participation_year']['label'] = '';
  $handler->display->display_options['fields']['field_mlog_participation_year']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_mlog_participation_year']['element_label_colon'] = FALSE;
  /* Field: Content: Month */
  $handler->display->display_options['fields']['field_month']['id'] = 'field_month';
  $handler->display->display_options['fields']['field_month']['table'] = 'field_data_field_month';
  $handler->display->display_options['fields']['field_month']['field'] = 'field_month';
  $handler->display->display_options['fields']['field_month']['label'] = '';
  $handler->display->display_options['fields']['field_month']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_month']['alter']['text'] = '[field_month] [field_mlog_participation_year]    [edit_node]';
  $handler->display->display_options['fields']['field_month']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_month']['alter']['path'] = '[field_monthly_log_file]';
  $handler->display->display_options['fields']['field_month']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_month']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_month']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Author uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'node';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = '';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uid']['link_to_user'] = FALSE;
  /* Field: Content: Monthly Log Status */
  $handler->display->display_options['fields']['field_monthly_log_status']['id'] = 'field_monthly_log_status';
  $handler->display->display_options['fields']['field_monthly_log_status']['table'] = 'field_data_field_monthly_log_status';
  $handler->display->display_options['fields']['field_monthly_log_status']['field'] = 'field_monthly_log_status';
  $handler->display->display_options['fields']['field_monthly_log_status']['label'] = '';
  $handler->display->display_options['fields']['field_monthly_log_status']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_monthly_log_status']['type'] = 'editable';
  $handler->display->display_options['fields']['field_monthly_log_status']['settings'] = array(
    'click_to_edit' => 0,
    'empty_text' => '',
    'fallback_format' => 'list_default',
  );
  /* Field: Realname: Real name */
  $handler->display->display_options['fields']['realname']['id'] = 'realname';
  $handler->display->display_options['fields']['realname']['table'] = 'realname';
  $handler->display->display_options['fields']['realname']['field'] = 'realname';
  $handler->display->display_options['fields']['realname']['relationship'] = 'user';
  $handler->display->display_options['fields']['realname']['label'] = '';
  $handler->display->display_options['fields']['realname']['exclude'] = TRUE;
  $handler->display->display_options['fields']['realname']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['realname']['link_to_user'] = FALSE;
  /* Sort criterion: Content: Month (field_month) */
  $handler->display->display_options['sorts']['field_month_tid']['id'] = 'field_month_tid';
  $handler->display->display_options['sorts']['field_month_tid']['table'] = 'field_data_field_month';
  $handler->display->display_options['sorts']['field_month_tid']['field'] = 'field_month_tid';
  /* Contextual filter: Profile: User uid */
  $handler->display->display_options['arguments']['user']['id'] = 'user';
  $handler->display->display_options['arguments']['user']['table'] = 'profile';
  $handler->display->display_options['arguments']['user']['field'] = 'user';
  $handler->display->display_options['arguments']['user']['relationship'] = 'field_pt_auth_monthly_log_target_id';
  $handler->display->display_options['arguments']['user']['default_action'] = 'default';
  $handler->display->display_options['arguments']['user']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['user']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['user']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['user']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['user']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['user']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['user']['validate']['type'] = 'user';
  $handler->display->display_options['arguments']['user']['validate_options']['restrict_roles'] = TRUE;
  $handler->display->display_options['arguments']['user']['validate_options']['roles'] = array(
    4 => '4',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'monthly_log' => 'monthly_log',
  );
  /* Filter criterion: Content: Participating School Year (field_mlog_participation_year) */
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['id'] = 'field_mlog_participation_year_value';
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['table'] = 'field_data_field_mlog_participation_year';
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['field'] = 'field_mlog_participation_year_value';
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['value'] = array(
    3 => '3',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: View area */
  $handler->display->display_options['header']['view']['id'] = 'view';
  $handler->display->display_options['header']['view']['table'] = 'views';
  $handler->display->display_options['header']['view']['field'] = 'view';
  $handler->display->display_options['header']['view']['view_to_insert'] = 'monthly_log_link:default';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'monthly_log' => 'monthly_log',
  );
  /* Filter criterion: Content: Participating School Year (field_mlog_participation_year) */
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['id'] = 'field_mlog_participation_year_value';
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['table'] = 'field_data_field_mlog_participation_year';
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['field'] = 'field_mlog_participation_year_value';
  $handler->display->display_options['filters']['field_mlog_participation_year_value']['value'] = array(
    7 => '7',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_monthly_log_file' => 'field_monthly_log_file',
    'field_month' => 'field_month',
    'uid' => 'uid',
    'field_monthly_log_status' => 'field_monthly_log_status',
    'realname' => 'realname',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_monthly_log_file' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_month' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_monthly_log_status' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'realname' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Monthly Log File */
  $handler->display->display_options['fields']['field_monthly_log_file']['id'] = 'field_monthly_log_file';
  $handler->display->display_options['fields']['field_monthly_log_file']['table'] = 'field_data_field_monthly_log_file';
  $handler->display->display_options['fields']['field_monthly_log_file']['field'] = 'field_monthly_log_file';
  $handler->display->display_options['fields']['field_monthly_log_file']['label'] = '';
  $handler->display->display_options['fields']['field_monthly_log_file']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_monthly_log_file']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_monthly_log_file']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_monthly_log_file']['type'] = 'file_url_plain';
  /* Field: Content: Participating School Year */
  $handler->display->display_options['fields']['field_mlog_participation_year']['id'] = 'field_mlog_participation_year';
  $handler->display->display_options['fields']['field_mlog_participation_year']['table'] = 'field_data_field_mlog_participation_year';
  $handler->display->display_options['fields']['field_mlog_participation_year']['field'] = 'field_mlog_participation_year';
  $handler->display->display_options['fields']['field_mlog_participation_year']['label'] = 'Year';
  /* Field: Content: Month */
  $handler->display->display_options['fields']['field_month']['id'] = 'field_month';
  $handler->display->display_options['fields']['field_month']['table'] = 'field_data_field_month';
  $handler->display->display_options['fields']['field_month']['field'] = 'field_month';
  $handler->display->display_options['fields']['field_month']['label'] = '';
  $handler->display->display_options['fields']['field_month']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_month']['alter']['path'] = '[field_monthly_log_file]';
  $handler->display->display_options['fields']['field_month']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_month']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_month']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Author uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'node';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = '';
  $handler->display->display_options['fields']['uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['uid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['uid']['link_to_user'] = FALSE;
  /* Field: Content: Monthly Log Status */
  $handler->display->display_options['fields']['field_monthly_log_status']['id'] = 'field_monthly_log_status';
  $handler->display->display_options['fields']['field_monthly_log_status']['table'] = 'field_data_field_monthly_log_status';
  $handler->display->display_options['fields']['field_monthly_log_status']['field'] = 'field_monthly_log_status';
  $handler->display->display_options['fields']['field_monthly_log_status']['label'] = '';
  $handler->display->display_options['fields']['field_monthly_log_status']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_monthly_log_status']['settings'] = array(
    'click_to_edit' => 0,
    'click_to_edit_style' => 'button',
    'empty_text' => '',
    'fallback_format' => 'list_default',
  );
  /* Field: Realname: Real name */
  $handler->display->display_options['fields']['realname']['id'] = 'realname';
  $handler->display->display_options['fields']['realname']['table'] = 'realname';
  $handler->display->display_options['fields']['realname']['field'] = 'realname';
  $handler->display->display_options['fields']['realname']['relationship'] = 'user';
  $handler->display->display_options['fields']['realname']['label'] = '';
  $handler->display->display_options['fields']['realname']['exclude'] = TRUE;
  $handler->display->display_options['fields']['realname']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['realname']['link_to_user'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Year (field_fact_doc_year) */
  $handler->display->display_options['sorts']['field_fact_doc_year_tid']['id'] = 'field_fact_doc_year_tid';
  $handler->display->display_options['sorts']['field_fact_doc_year_tid']['table'] = 'field_data_field_fact_doc_year';
  $handler->display->display_options['sorts']['field_fact_doc_year_tid']['field'] = 'field_fact_doc_year_tid';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Profile: User uid */
  $handler->display->display_options['arguments']['user']['id'] = 'user';
  $handler->display->display_options['arguments']['user']['table'] = 'profile';
  $handler->display->display_options['arguments']['user']['field'] = 'user';
  $handler->display->display_options['arguments']['user']['relationship'] = 'field_pt_auth_monthly_log_target_id';
  $handler->display->display_options['arguments']['user']['default_action'] = 'default';
  $handler->display->display_options['arguments']['user']['default_argument_type'] = 'user';
  $handler->display->display_options['arguments']['user']['default_argument_options']['user'] = FALSE;
  $handler->display->display_options['arguments']['user']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['user']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['user']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'monthly_log' => 'monthly_log',
  );
  $handler->display->display_options['path'] = 'user/%/monthly-logs-list';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Previous Monthly Logs';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['monthly_logs'] = $view;

  return $export;
}
