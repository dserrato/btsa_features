<?php
/**
 * @file
 * btsa_support_provider_contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function btsa_support_provider_contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_wide_support_providers_pts';
  $context->description = 'The PT\'s assigned to the current reader site wide context.';
  $context->tag = 'sp';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('The PT\'s assigned to the current reader site wide context.');
  t('sp');
  $export['site_wide_support_providers_pts'] = $context;

  return $export;
}
